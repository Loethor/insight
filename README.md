<img src="./docs/images/imagen.jpg" alt="logo" width="200" align="right" />

# Insight 
> A data pipeline for animal behavior analysis

Insight helps the user to analyze AI-generated animal data.


## Table of Contents

- [Context](#context)
- [What does Insight do?](#what-does-insight-do)
  - [Available plots](#available-plots)
  - [Conclusions on the plots](#conclusions-on-the-plots)
- [Installation](#installation)
  - [Requirements](#requirements)
  - [Python and Git](#python-and-git)
  - [Clone this repository](#clone-this-repository)
  - [Create a virtual environment](#(optional))
  - [Install the required dependencies](#install-the-required-dependencies)
- [Usage](#usage)
  - [Required input](#required-input)
  - [Configuration file](#configuration-file)
  - [How to run](#how-to-run)
  - [Expected Output](#expected-output)
- [Contact information](#contact-information)



## Context

This repository contains Insight, a data pipeline developed for [IMAGEN](https://www.wur.nl/en/show/Imagine-all-the-animals-living-life-in-sociable-groups.htm), an NWO-funded project carried out by a consortium of Dutch companies and universities. IMAGEN aims to understand animal social interactions (good and bad) and link these interaction to individual genes. This way, the breeding process can be improve to produce "super social animals" which will result in a better welfare of the livestock animals.

 This pipeline was developed by Luis Roma Barge ([Eindhoven University of Technology](https://www.tue.nl/en/)) during his PDEng final project. This project had the title **"The design of the Insight pipeline for behavioral animal science & animal breeding"** and the resulting PDEng thesis can be found [here](https://research.tue.nl/en/publications/the-design-of-the-insight-pipeline-for-behavioral-animal-science-).

## What does Insight do?
Insight read and analyzes AI-generated data (e.g., obtained from using Computer Vision methods such as YOLOv3) in order to produce useful analytics for researchers and breeders. Some use cases for Insight include:
 - Measuring the distance traveled by the animals 
  - Analyzing the movement trajectory of the animals
  - Studying social contacts of pairs of animals
  - Analyzing the movement and velocity of the animals, among other significative parameters

### Available plots
Insight can generate multiple plots that help the user to visualize important features such as trajectories, proximity, velocities, and social interation in animal data. Some examples are show in the following subsection.


#### Trajectory plot

The first plot Insight produces is a trajectory plot. In this plot, the x-axis and y-axis represent the horizontal and vertical distances of the camera’s field of view in pixels. The lines on the plot are the trajectories of individual pigs, i.e., trajectories of the centers of the bounding boxes for each animal ID. Each animal has its trajectory drawn with a different color. The starting point of each trajectory is marked with a red circle and the ending point is marked with a black star. 

<img src="./docs/images/trajectories.png" alt="logo" width="400" />

This plot can also be obtained for a subset of animals if configured in the configuration file.

<img src="./docs/images/trajectories-subset.png" alt="logo" width="400" />


#### Distance plot

Given the trajectory, the system can also determine the total distance traveled by each individual pig. This is an important metric for farmers and breeders. Breeders care about distance traveled because the activity of a pig is related to his feeding efficiency. The more that a pig moves, the more food it needs to grow. 
Insight obtains the distance traveled and plots it in a bar plot shown below. This plot shows the individual distance traveled per animal in pixel units.

<img src="./docs/images/distance.png" alt="logo" width="400" />

This plot can also be obtained for a subset of animals if configured in the configuration file.

<img src="./docs/images/distance-subset.png" alt="logo" width="400" />


#### Heatmap proximity plot

The proximity between animals is a crucial factor when analyzing aggressive or positive behavior. In Insight, multiple plots are available to visualize the proximity between certain pairs of animals.  For each pair of animals, Insight calculates the distance between them and counts in how many frames this distance is under a predefined threshold. 

The number of close encounters between each pair of animals defines a (symmetric) adjacency matrix. This matrix can be visualized with a Heatmap plot as shown below. In this figure, each row and column correspond to a pig and the number in the intersection is the number of frames these two pigs are near each other (e.g., Animal 1 and 3 are close to each other in 38 frames). The diagonal (i.e., where two equal IDs intersect) must be ignored as it has no meaning.

<img src="./docs/images/heatmap.png" alt="logo" width="400" />


#### Social Network plot

One of the motivations of Insight is to obtain visual information that is understandable. Due to this, plots that can be understood with a single glance are preferred. Insight combines the aforementioned adjacency matrix with velocity information (obtained from the differences in the bounding boxes throughout multiple frames) to generate a social network. This social network is represented as a graph [29] where the nodes represent the pigs and the edges between them represent interactions. 

The Graph plot presented below provides information in two additional ways, color and size. The nodes are colored based on the state of the pig and the edges connecting these nodes have their color and size variables depending on the number of interactions. 

<img src="./docs/images/network.png" alt="logo" width="400" />

#### Timeline plot

If there is a need for more precise information about the states of the pigs, the plots show below can be proven useful.

<img src="./docs/images/timeline.png" alt="logo" width="400" />

In this figure, each of the subplots represents a timeline of states during the scene for a specific pig. By analyzing their instant velocity and position on each frame it can be estimated if the animal is moving, sitting, or eating. For example, Pig 10 is eating during the scene while Pig 7 alternates between moving and sitting.

### Conclusions on the plots

All these plots are the results of an iterative process of development and validation. The resulting plots have been selected by animal behavior researchers as useful and clarifying. These plots allow them to focus on individual details or specific features to further analyze the behavior of the animals.


## Installation
Insight is purely build on Python. It works with **Python 3.8** and **Python 3.9**. Future Python version have not been proven to work, it finally depend of the internal libraries that Insight uses and their compatibility with future version of Python.

### Requirements
Some Python libraries that Insight uses:
 - MatPlotLib
 - Networkx
 - Pandas
 - Numpy
 - PyYAML
 - OpenCV

The list of requirements can be found on the [requirements.txt](./requirements.txt) file.

### Python and Git
The following steps ensure the proper installation of Insight. These steps assume the previous installation of Python (3.8 or 3.9). If this is not the case, it can be obtained [here](https://www.python.org/downloads/). It is also assumed the installation of git in order to clone this repository. Git (for Windows) can be found [here](https://git-scm.com/download/win).

### Clone this repository
Open the command line (cmd.exe) in the desired installation folder.

Execute the following command:

```
git clone https://gitlab.com/Loethor/insight
```

This will download this repository in your machine.

### (Optional but recommended) Create a virtual environment
It is recommended to have a separated virtual environment for each of your projects. More information about virtual environments can be found [here](https://docs.python.org/3/library/venv.html). With a virtual environment you can install different modules and python version for different projects.

### Install the required dependencies
For installing new python packages/modules we recommend [pip](https://pypi.org/project/pip/). 

You can obtain pip using the following command:

`
python get-pip.py
`

You may need to restart your command line.

To install the dependencies required for Insight execute the following command:

```
pip install -r requirements.txt
```

This command installs all the required dependencies that are included in said file.


## Usage

### Required input
In order to work Insight requires data. Data must be placed in the [data](./data) folder. Inside of the data file you can create different subfolders, depending on the kind of animal you want to analyzed. As an example, there are two examples, one for [chickens](./data/chicken) and another for [pigs](./data/pig).

Inside of these subfolders you must include a file that contains the bounding boxes of the animals that you want to analyze.
The expected format is a csv-like file with the following fields:

` Frame ID Left Up Width Height`

Where Frame is the number of the frame, ID is the identification number of the animal, and Left, Up, Width, Height are the coordinates of the bounding boxes and their dimensions.

A example of this file can be found [here](./data/pig/boundingbox.txt). 
The file has the following appearance:

![Example](./examples/example-input-data.PNG)


### Configuration file
In order to select with analytics to obtain, Insight must be configured with a configuration file. This configuration file can be found [here](./configuration.yml) and it has YAML format for its ease of use.

An example of this configuration file:

![Configuration](./examples/configuration.png)

In this configuration file you can select option such as which type of animal to analyze, which animals (ID) to analyze, which plots to obtain, and a searching radius (used for some plots).

In this file you can **deselect** options by adding a `#` before the line.


### How to run
In the project folder execute:

```
python main.py
```

This command will generate (on screen and in output folder) the analytics selected in the configuration file.





## Contact information

Any question can be forwarded to this email lrb2392@gmail.com .
