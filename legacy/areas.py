
def generate_areas(self):
    
    
    id = 2
    fig, (ax1) = plt.subplots(nrows = 1, ncols = 1)
    for animal in self.desired_animal_list:
        if animal.id==id:
            selected_animal=animal
            
    areas = selected_animal.areas
    
    ax1.plot(areas, "bo", label = "Bounding box area")
    
    ax1.set_title('Animal Id {}'.format(selected_animal.id))
    ax1.set(xlabel='Frame', ylabel="Area (U2)")
    plt.show()
    self.plot_areas(id)
    
def plot_areas(self, id):
    import matplotlib.patches as patches
    
    
    fig, (ax1) = plt.subplots(nrows = 1, ncols = 1)
    for animal in self.desired_animal_list:
        if animal.id==id:
            selected_animal=animal
            break
        
    sa = selected_animal   
            
    areas = sa.areas
    
    def update(frame):
            
        ax1.clear()
        
        
        
        l = sa.bb_l[frame]
        t = sa.bb_t[frame]
        w = sa.bb_w[frame]
        h = sa.bb_h[frame]
        
        rect = patches.Rectangle((t, l), w, h, linewidth=1, edgecolor='r', facecolor='none')
        ax1.add_patch(rect)
        
            
        img = plt.imread(obtain_image_path(frame+1,self.images_path))
        ax1.imshow(img)
        ax1.set_title('Frame {}'.format(frame))
        return rect,
    
    ani = FuncAnimation(fig, update, frames=self.frame_count, interval=1000,
                blit=False, repeat=False)
    # plt.gca().invert_yaxis()
    ani.save("area{}.mp4".format(id), fps=1)
    plt.show()
    
    
    
    
    
def __create_plot_dictionary(self):
        
    plot_dict = {
        'static_trajectories': self.generate_trajectories,
        'dynamic_trajectories' : self.generate_live_trajectories,
        'distance_hist' : self.generate_distance_histogram,
        'distance_bar': self.generate_distance_bar,
        'proximity_seconds': self.generate_proximity_bar_in_seconds,
        'proximity_matrix': self.plot_proximity_matrix,
        'graph': self.plot_proximity_graph,
        'velocity_plot' : self.generate_velocity_plots,
        'states' : self.generate_states_plot,
        'state_timeline': self.generate_timeline,
        'areas': self.generate_areas
    }
    
    return plot_dict