from abc import ABC, abstractmethod
import matplotlib.pyplot as plt

from classes.plots.Plot import Plot


class StatesPlot(Plot):

    def __init__(self, input_data,  isSaved=False):
        super().__init__(input_data, isSaved)
        self.name = "StatesPlot"

        # # TODO later video duration and number of frames should be automated.
        # self.video_durantion = 138
        # self.number_of_frames = 2760
        # self.seconds_per_frame = self.video_duration / float(number_of_frames)

    def plot(self):
        super().plot()

        animals = self.input_data
        ids = []
        ones = []
        total_states = []

        eating = [0 for animal in animals]
        moving = [0 for animal in animals]
        sitting = [0 for animal in animals]

        frame_count = len(animals[0].x)
        animal_type = animals[0].type

        for animal in animals:
            ids.append(animal.id)
            ones.append(1)

        for animal in animals:
            id_ = animal.id-1

            for frame in range(frame_count):
                state = animal.concrete_states[frame]
                if state == "eating":
                    eating[id_] += 1
                if state == "moving":
                    moving[id_] += 1
                if state == "sitting":
                    sitting[id_] += 1

        sitting = [a for a in sitting]
        moving = [a for a in moving]
        eating = [a for a in eating]

        suma = [a+b for a, b in zip(moving, sitting)]
        suma2 = [a+b for a, b in zip(moving, eating)]

        width = 0.35
        self.fig, self.ax = plt.subplots()
        fig, ax = self.fig, self.ax
        ax.bar(ids, sitting, width, label='sitting')
        ax.bar(ids, moving, width, label='moving', bottom=sitting)
        ax.bar(ids, eating, width, label='eating', bottom=suma)

        plt.xticks(range(len(animals)+1))
        ax.set(xlabel='{} ID'.format(animal_type), ylabel="Frames on state")
        ax.set_title("{} states".format(animal_type))
        plt.legend()
        plt.show()

        if self.isSaved:
            self.save(self.fig, '.png')
