from classes.plots.Plot import Plot
import matplotlib.pyplot as plt


class StaticTrajectoriesPlot(Plot):

    def __init__(self, input_data, isSaved=False):
        super().__init__(input_data, isSaved)
        self.name = "StaticTrajectoriesPlot"

    def plot(self):
        super().plot()
        self.fig, self.ax = plt.subplots(nrows=1, ncols=1)
        desired_animal_list = self.input_data
        animal_type = desired_animal_list[0].type

        ax = self.ax

        for animal in desired_animal_list:
            ax.plot(animal.x[0], animal.y[0], 'ro')
            ax.plot(animal.x, animal.y, label="{} {}".format(animal_type, animal.id))
            ax.plot(animal.x[-1], animal.y[-1], 'k*')

        ax.set_title('{}s\' trajectory'.format(animal_type))
        ax.set(xlabel='x (pixels)', ylabel="y (pixels)")
        ax.legend(loc='best')

        plt.gca()
        plt.show()

        if self.isSaved:
            self.save(self.fig, '.png')
