from abc import ABC, abstractmethod
import matplotlib.pyplot as plt

from classes.plots.Plot import Plot


class ProximityBarPlot(Plot):

    def __init__(self, input_data,  isSaved=False):
        super().__init__(input_data, isSaved)
        self.name = "ProximityBarPlot"

        # # TODO later video duration and number of frames should be automated.
        # self.video_durantion = 138
        # self.number_of_frames = 2760
        # self.seconds_per_frame = self.video_duration / float(number_of_frames)

    def plot(self):
        super().plot()
        self.fig, self.ax = plt.subplots()

        desired_animal_list, proximity, radius = self.input_data
        animal_type = desired_animal_list[0].type

        animal_indices = []
        for animal in desired_animal_list:
            animal_indices.append(animal.id)

        count = len(desired_animal_list)
        self.ax.bar(animal_indices, proximity)

        plt.xticks(range(len(desired_animal_list)+1))
        plt.xlabel('{} Id'.format(animal_type), fontsize=15)
        # plt.ylabel('Time near another Animal (s)',fontsize=15)
        plt.ylabel('Frames near another {} (s)'.format(
            animal_type), fontsize=15)
        plt.title('Radius {}'.format(radius))
        plt.show()

        if self.isSaved:
            self.save(self.fig, '.png')
