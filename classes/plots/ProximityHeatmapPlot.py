from abc import ABC, abstractmethod
import matplotlib.pyplot as plt

from classes.plots.Plot import Plot
import numpy as np


class ProximityHeatmapPlot(Plot):

    def __init__(self, input_data,  isSaved=False):
        super().__init__(input_data, isSaved)
        self.name = "ProximityHeatmapPlot"

        # # TODO later video duration and number of frames should be automated.
        # self.video_durantion = 138
        # self.number_of_frames = 2760
        # self.seconds_per_frame = self.video_duration / float(number_of_frames)

    def plot(self):
        super().plot()
        self.fig, self.ax = plt.subplots()
        fig = self.fig
        ax = self.ax

        animal_matrix, radius = self.input_data

        count = animal_matrix.shape[0]

        im = ax.imshow(animal_matrix)

        # We want to show all ticks...
        ax.set_xticks(np.arange((count)))
        ax.set_yticks(np.arange((count)))
        # ... and label them with the respective list entries
        ax.set_xticklabels(range(1, count+1))
        ax.set_yticklabels(range(1, count+1))

        # Rotate the tick labels and set their alignment.
        plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
                 rotation_mode="anchor")

        # Loop over data dimensions and create text annotations.
        for i in range((count)):
            for j in range((count)):
                text = ax.text(j, i, animal_matrix[i, j],
                               ha="center", va="center", color="w")

        # ax.set_title("Time close to another animal (s) Radius {}".format(radius))
        ax.set_title(
            "# Frames close to another animal - Radius {}".format(radius))
        fig.tight_layout()
        plt.gca().invert_yaxis()
        plt.show()

        if self.isSaved:
            self.save(self.fig, '.png')
