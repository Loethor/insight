from classes.plots.Plot import Plot
from classes.auxiliary import obtain_image_path

import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import cv2


class LiveTrajectoriesPlot(Plot):

    def __init__(self, input_data, isSaved=False, images_path=None):
        super().__init__(input_data, isSaved)
        self.name = "LiveTrajectoriesPlot"
        self.images_path = images_path

    def plot(self):
        super().plot()
        self.fig, self.ax = plt.subplots(nrows=1, ncols=1)

        desired_animal_list = self.input_data

        ax = self.ax

        LN = []
        YDATA = []
        XDATA = []

        indices = []

        frame_count = len(desired_animal_list[0].x)

        for animal in desired_animal_list:
            indices.append(animal.id)

        desired_count = len(desired_animal_list)
        for _ in range(desired_count):
            ln, = ax.plot([], [])
            LN.append(ln)
            XDATA.append([])
            YDATA.append([])

        def update(frame):

            resampled = False

            img = plt.imread(obtain_image_path(frame+1, self.images_path))
            if img.shape[0] > 800:
                img = cv2.resize(img, None, fx=1/3, fy=1/3,
                                 interpolation=cv2.INTER_AREA)
                resampled = True

            for animal, xdata, ydata in zip(desired_animal_list, XDATA, YDATA):
                if resampled:
                    xdata.append(animal.x[frame]/3)
                    ydata.append(animal.y[frame]/3)
                else:
                    xdata.append(animal.x[frame])
                    ydata.append(animal.y[frame])

            for ln, xdata, ydata in zip(LN, XDATA, YDATA):
                ln.set_data(xdata, ydata)

            ax.imshow(img)
            # del img

            if animal == "Pig":
                plt.plot(200, 125, 'ro')

            # plt.show()

            return tuple(LN)

        if frame_count > 30:
            print("Too many frames in the animation, limiting it to 30.")
            frame_count = 30

        ani = FuncAnimation(self.fig, update, frames=frame_count, interval=1000,
                            blit=False, repeat=False, save_count=0)

        plt.gca().invert_yaxis()
        # plt.show()
        if self.isSaved:
            self.save('.mp4', ani, indices)

    def save(self, extension, animation, indices):

        ind_str = [str(ind) for ind in indices]
        str_of_ints = "_".join(ind_str)

        name = 'output/' + self.name + '_' + str(str_of_ints) + extension
        animation.save(name, fps=3)
