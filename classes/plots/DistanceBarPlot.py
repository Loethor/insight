from classes.plots.Plot import Plot

import matplotlib.pyplot as plt


class DistanceBarPlot(Plot):

    def __init__(self, input_data,  isSaved=False):
        super().__init__(input_data, isSaved)
        self.name = "DistanceBarPlot"

    def plot(self):
        super().plot()
        self.fig, self.ax = plt.subplots(nrows=1, ncols=1)

        desired_animal_list = self.input_data

        animal_type = desired_animal_list[0].type

        ax = self.ax

        distances = []
        ids = []

        for animal in desired_animal_list:
            distances.append(animal.distance)
            ids.append(animal.id)

        ax.bar(ids, distances)
        #TODO
        
        if len(desired_animal_list)<desired_animal_list[-1].id:
            plt.xticks(range(desired_animal_list[-1].id+1))
        else:
            plt.xticks(range(len(desired_animal_list)+1))
        plt.xlabel('{} Id'.format(animal_type), fontsize=15)
        plt.ylabel('Distance (pixels)', fontsize=15)
        plt.title('Total distance traveled', fontsize=15)

        plt.gca()
        plt.show()

        if self.isSaved:
            self.save(self.fig, '.png')
