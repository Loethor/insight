from abc import ABC, abstractmethod
import matplotlib.pyplot as plt

from classes.plots.Plot import Plot
import networkx as nx
import numpy as np


class ProximityGraphPlot(Plot):

    def __init__(self, input_data,  isSaved=False):
        super().__init__(input_data, isSaved)
        self.name = "ProximityGraphPlot"

        # # TODO later video duration and number of frames should be automated.
        # TODO graph with not all
        # self.video_durantion = 138
        # self.number_of_frames = 2760
        # self.seconds_per_frame = self.video_duration / float(number_of_frames)

    def plot(self):
        super().plot()
        self.fig, self.ax = plt.subplots()

        desired_animal_list, animal_matrix, radius = self.input_data
        count = animal_matrix.shape[0]
        animal_type = desired_animal_list[0].type

        G = nx.Graph()

        for i in range(1, count + 1):
            G.add_node(i)

        a, b = animal_matrix.shape

        for i in range(0, a):
            for j in range(0, b):
                if animal_matrix[i, j] > 0:
                    G.add_edge(i+1, j+1, weight=animal_matrix[i, j])
        biggest = np.max(np.max(animal_matrix))
        # nx.draw(G, with_labels=True, font_weight='bold')
        # plt.show()

        esmall = [(u, v) for (u, v, d) in G.edges(
            data=True) if d["weight"] < biggest / 3]
        medium = [(u, v) for (u, v, d) in G.edges(data=True) if d["weight"]
                  >= biggest / 3 and d["weight"] < biggest * 2 / 3]
        large = [(u, v) for (u, v, d) in G.edges(data=True)
                 if d["weight"] >= biggest * 2 / 3]

        # pos = nx.spring_layout(G)  # positions for all nodes
        pos = nx.circular_layout(G)  # positions for all nodes
        # pos = nx.kamada_kawai_layout(G)  # positions for all nodes

        # coloring based in activity

        passive = []
        active = []
        eating = []
        very_active = []
        for animal in desired_animal_list:
            if animal.overall_state == 'passive':
                passive.append(animal.id)
            if animal.overall_state == 'active':
                active.append(animal.id)
            if animal.overall_state == 'eating':
                eating.append(animal.id)
            if animal.overall_state == 'very_active':
                very_active.append(animal.id)

        # nodes

        # nx.draw_networkx_nodes(G, pos, node_size=700)

        nx.draw_networkx_nodes(G, pos, nodelist=very_active,
                               node_color="r", node_size=700, label="very_active")
        nx.draw_networkx_nodes(G, pos, nodelist=active,
                               node_color="g", node_size=700, label="active")
        nx.draw_networkx_nodes(G, pos, nodelist=eating,
                               node_color="b", node_size=700, label="eating")
        nx.draw_networkx_nodes(G, pos, nodelist=passive,
                               node_color="grey", node_size=700, label="passive")

        # edges
        nx.draw_networkx_edges(G, pos, edgelist=large,
                               width=8, edge_color="r", label='large contact')
        nx.draw_networkx_edges(G, pos, edgelist=medium,
                               width=6, edge_color="y", label='medium contact')
        # nx.draw_networkx_edges(
        # G, pos, edgelist=esmall, width=4, alpha=0.5, edge_color="b", style="dashed"
        # )
        nx.draw_networkx_edges(G, pos, edgelist=esmall, width=4,
                               alpha=0.5, edge_color="g", label='small contact')

        # labels
        nx.draw_networkx_labels(G, pos, font_size=20,
                                font_family="sans-serif", font_color='w')

        lgnd = plt.legend(loc='best')
        lgnd.legendHandles[0]._sizes = [80]
        lgnd.legendHandles[1]._sizes = [80]
        lgnd.legendHandles[2]._sizes = [80]
        lgnd.legendHandles[3]._sizes = [80]

        plt.title("{} contacts under radius of {}".format(animal_type, radius))
        plt.axis("off")
        plt.show()

        if self.isSaved:
            self.save(self.fig, '.png')
