from abc import ABC, abstractmethod
import matplotlib.pyplot as plt


class Plot(ABC):
    def __init__(self, input_data=None, isSaved=False):
        self.input_data = input_data
        self.name = "Template"

        self.isSaved = isSaved

    def plot(self):
        print("Plotting {}".format(self.name))

    def save(self, fig, extension):
        name = 'output/' + self.name + extension
        fig.savefig(name)

    def destroy(self):
        plt.close('all')
        del self.fig, self.ax
