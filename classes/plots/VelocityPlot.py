from abc import ABC, abstractmethod
import matplotlib.pyplot as plt

from classes.plots.Plot import Plot
import numpy as np


class VelocityPlot(Plot):

    def __init__(self, input_data,  isSaved=False):
        super().__init__(input_data, isSaved)
        self.name = "VelocityPlot"

    def plot(self):
        super().plot()
        animals = self.input_data

        animal_type = animals[0].type

        self.fig, ((self.ax1, self.ax2), (self.ax3, self.ax4)
                   ) = plt.subplots(nrows=2, ncols=2)
        fig = self.fig
        ax1 = self.ax1
        ax2 = self.ax2
        ax3 = self.ax3
        ax4 = self.ax4

        t = [t for t in range(len(animals[0].x))]

        avg_speed = []
        velocities = []
        ids = []
        for animal in animals:
            ax1.plot(t[:-1], animal.v,'.', label="Animal {}".format(animal.id))
            avg_speed.append(animal.avg_v)
            velocities.append(animal.v)
            ids.append(animal.id)

        ax1.set(xlabel='Frame', ylabel='{} velocity (u/s)'.format(animal_type))
        ax2.set(xlabel='{} ID'.format(animal_type),
                ylabel="Average velocity (u/s)")

        ax2.plot(ids, avg_speed, 'bo')

        ax1.set_title('Individual velocities')
        ax2.set_title('Average velocities')

        ax1.legend(loc='best')

        def normal_dist(x, mean, sd):
            prob_density = (1/(np.sqrt(2*np.pi)*sd)) * \
                np.exp(-0.5*((x-mean)/sd)**2)
            return prob_density

        mean_avg_speed = np.mean(avg_speed)
        std_avg_speed = np.std(avg_speed)

        x2 = np.max(avg_speed)
        x = np.linspace(0, x2+10, 100)
        pdf_data = normal_dist(avg_speed, mean_avg_speed, std_avg_speed)
        pdf_fit = normal_dist(x, mean_avg_speed, std_avg_speed)

        a1 = mean_avg_speed-std_avg_speed
        a2 = mean_avg_speed + std_avg_speed
        f1 = normal_dist(a1, mean_avg_speed, std_avg_speed)
        f2 = normal_dist(a2, mean_avg_speed, std_avg_speed)


        # Plotting the Results
        ax4.plot(avg_speed, pdf_data, '.b', label='data')
        ax4.plot(x, pdf_fit, '-k',linewidth=0.5, label='fit')
        ax4.plot(a1, f1, '+r', label='fit')
        ax4.plot(a2, f2, '+r', label='fit')
        ax4.set(xlabel='Average Velocity', ylabel='Probability Density')

        flat_velocities = [item for sublist in velocities for item in sublist]
        mean_flat_velocities = np.mean(flat_velocities)
        std_flat_velocities = np.std(flat_velocities)
        
        x2 = np.max(flat_velocities)
        x = np.linspace(0, x2+10, 100)
        a1 = mean_flat_velocities - std_flat_velocities
        a2 = mean_flat_velocities + std_flat_velocities
        f1 = normal_dist(a1, mean_flat_velocities, std_flat_velocities)
        f2 = normal_dist(a2, mean_flat_velocities, std_flat_velocities)

        pdf2_data = normal_dist(
            flat_velocities, mean_flat_velocities, std_flat_velocities)
        pdf2_fit = normal_dist(x, mean_flat_velocities, std_flat_velocities)

        
        ax3.plot(flat_velocities, pdf2_data, '.b', label='data')
        ax3.plot(x, pdf2_fit, '-k', linewidth=0.5, label='fit')
        ax3.set(xlabel='Velocity', ylabel='Probability Density')

        plt.show()

        if self.isSaved:
            self.save(self.fig, '.png')

    def destroy(self):
        plt.close('all')
        del self.fig, self.ax1, self.ax2
