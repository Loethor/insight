from abc import ABC, abstractmethod
import matplotlib.pyplot as plt
from matplotlib.collections import PolyCollection


from classes.plots.Plot import Plot


class StateTimelinePlot(Plot):

    def __init__(self, input_data,  isSaved=False):
        super().__init__(input_data, isSaved)
        self.name = "StateTimelinePlot"

        # # TODO later video duration and number of frames should be automated.
        # self.video_durantion = 138
        # self.number_of_frames = 2760
        # self.seconds_per_frame = self.video_duration / float(number_of_frames)

    def plot(self):
        super().plot()

        animals, selected_animal_id = self.input_data

        for animal in animals:
            if animal.id == selected_animal_id:
                selected_animal = animal

        concrete_states = selected_animal.concrete_states

        frame_count = len(animals[0].x)

        data = []

        for frame in range(frame_count):
            data.append((frame, frame+1, concrete_states[frame]))

        cats = {"sitting": 1, "moving": 2, "eating": 3}
        colormapping = {"sitting": "C0", "moving": "C1", "eating": "C2"}

        verts = []
        colors = []
        for d in data:
            v = [(d[0], cats[d[2]]-.4),
                 (d[0], cats[d[2]]+.4),
                 (d[1], cats[d[2]]+.4),
                 (d[1], cats[d[2]]-.4),
                 (d[0], cats[d[2]]-.4)
                 ]
            verts.append(v)
            colors.append(colormapping[d[2]])

        bars = PolyCollection(verts, facecolors=colors)

        fig, ax = plt.subplots()
        self.fig = fig
        self.ax = ax

        ax.add_collection(bars)
        ax.autoscale()

        ax.set(xlabel='Frame')
        ax.set_yticks([1, 2, 3])
        ax.set_yticklabels(["sitting", "moving", "eating"])
        plt.title("animal ID: {}".format(selected_animal.id))
        plt.show()

    def save(self, extension):
        name = 'output/' + self.name + extension
        plt.savefig(name)

    def destroy(self):
        plt.close('all')
        del self.fig, self.ax
